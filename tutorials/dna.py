from collections import Counter


def clean_up(data):
    return "".join(data.split("\n")[2:]).replace(" ", "")


def clean_up2(data: str):
    return "".join([x for x in
                    [x
                     for line in data.split("\n") if not line.startswith('>')
                     for x in line]
                    if x in ['A', 'T', 'C', 'G']])


def gc_content(data: str):
    ctr = Counter(data)
    return (ctr["G"] + ctr["C"]) / len(data)


def list_kmers(s, k):
    return [s[i:i + k] for i in range(len(s) - k + 1)]


def number_of_unique_kmers(s, k):
    return len([x for x, i in Counter(list_kmers(s, k)).items() if i == 1])


def kmer_code(kmer: str):
    digits = kmer.translate(str.maketrans("ACGT", "0123"))
    result = 0
    for i in range(len(digits)):
        result += int(digits[i])*(4**(len(digits) - i - 1))
    return result


def reverse_complement(data: str):
    return data.translate(str.maketrans("ATCG", "TAGC"))[::-1]


def canonical_code(kmer):
    return max(kmer_code(kmer), kmer_code(reverse_complement(kmer)))


# Tests
def test_number_of_unique_kmers():
    assert number_of_unique_kmers("AAAA", 2) == 0
    assert number_of_unique_kmers("AAACD", 3) == 3


def test_list_mers():
    mers = list_kmers("AAAACD", 3)
    assert set(mers) == {"AAA", "ACD", "AAC"}
    assert len(mers) == 4


def test_kmer_code():
    assert kmer_code("ACGT") == 27


def test_reverse_complement():
    assert reverse_complement("AAAC") == "GTTT"


def test_canonical_code():
    assert canonical_code("AAAC") == 191


if __name__ == '__main__':
    raw_data = """
    >ENA|AAF26411|AAF26411.1 TGEV spike protein expression construct truncated spike p
    ATGGCTTTCTTGAAGAGTTTCCCATTCTACGCTTTCTTGTGCTTCGGACAATACTTCGTG
    GCTGTGACTCACGCAGACAACTTCCCATGCTCTAAGTTGACCAACAGGACCATCGGTAAT...
    """
    dna = clean_up2(raw_data)
    print(dna)
    print(gc_content(dna))
    print([1, 2, 3][::-1])
