# parse file
with open("sequence.fasta", "rt") as sequence_file:
    sequence = "".join(sequence_file.readlines()[1:]).replace("\n", "")

# parse genetic code
with open("04-genetic-code.txt", "rt") as codes_file:
    codes = dict()
    for line in codes_file:
        pair = line.split(":")
        for codon in pair[1].split():
            codes[codon] = pair[0]


def strip(input_sequence: str):
    input_sequence = input_sequence[input_sequence.find("ATG"):]  # strip to start
    acid = ""
    for i in range(0, len(input_sequence), 3):
        splice = codes.get(input_sequence[i:i + 3], "")
        if splice == "STOP":
            print(acid)
            strip(input_sequence[i:])
            break
        acid += splice


strip(sequence)
