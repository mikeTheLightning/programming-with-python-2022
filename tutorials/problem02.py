def my_max(numbers):
    if numbers:
        maximum = numbers[0]
        for number in numbers:
            if number > maximum:
                maximum = number
        return maximum
    else:
        return None


def process_list(numbers: list):
    for i in range(len(numbers)):
        if numbers[i] % 2 == 0:
            numbers[i] //= 2
        else:
            numbers[i] *= 2
        if i % 7 == 0:
            numbers[i] += i
    return max(numbers)


def collatz(number: int) -> int:
    done = []
    while number not in done:
        done.append(number)
        number = number // 2 if number % 2 == 0 else number * 3 + 1
    return done[-1]


def caesar(plaintext):
    ciphertext = ""
    for letter in plaintext:
        if ord("Z") >= ord(letter) >= ord("A"):
            ciphertext += chr((ord(letter) - ord("A") + 3) % 26 + ord("A"))
        else:
            ciphertext += letter
    return ciphertext


def fib(n):
    return fib_rec(n, 1, 1)


def fib_rec(n, i, j):
    if n < 1:
        return i
    else:
        return fib_rec(n - 1, j, i + j)


if __name__ == '__main__':
    example1 = [1, 4, -35, 69]
    print(f"my_max({example1}): {my_max(example1)}")
    print(f"process_list({example1}): {process_list(example1)}")
    print(f"example1: {example1}")
    print(f"collatz: {collatz(19)}")
    print(f"caesar:{caesar('HELLO WORLD')}")
    print(fib(5))
