from collections import Counter


def count_duplicates(input):
    letter_frequencies = Counter(input.lower())
    return len([x for x in letter_frequencies if letter_frequencies[x] > 1])


def multisum(digits):
    result = 1
    for digit in digits:
        result *= digit
    return result


def persistence(number):
    digits = [int(i) for i in str(number)]
    return 0 if len(digits) == 1 else persistence(multisum(digits)) + 1


def get_biggest_persistence():
    persistences = [persistence(i) for i in range(10000)]
    maximum = max(persistences)
    persistences.reverse()
    return maximum, 9999 - persistences.index(maximum)


def test_999():
    assert persistence(999) == 4


def test_count_duplicates():
    assert count_duplicates("abBcdea") == 2


def test_biggest_persistence():
    assert 6 == persistence(8876)


if __name__ == '__main__':
    print(count_duplicates("abBcdea"))
    print(persistence(999))
    print(get_biggest_persistence())
