class Monkey:
    def __init__(self, monkey_code):
        monkey_lines = monkey_code.split('\n')
        assert len(monkey_lines) == 6
        self.number = int(monkey_lines[0].split()[1][:1])
        self.items = list(map(lambda x: int(x[0:2]), monkey_lines[1].split()[2:]))
        operation_tokens = monkey_lines[2].split()
        self.operation = operation_tokens[4]
        self.operand = operation_tokens[5]
        self.test_value = int(monkey_lines[3].split()[3])
        self.partner_true = int(monkey_lines[4].split()[5])
        self.partner_false = int(monkey_lines[5].split()[5])
        self.items_inspected = 0

    def operate(self, item):
        value = item if self.operand == 'old' else int(self.operand)
        match self.operation:
            case '+':
                return item + value
            case '*':
                return item * value

    def throw(self, item) -> int:
        return self.partner_true if item % self.test_value == 0 else self.partner_false


def execute_round(monkeys, mod):
    """One round of monkeys throwing stuff"""
    for monkey in monkeys:
        for item in monkey.items:
            if mod:
                new_item = monkey.operate(item) % mod
            else:
                new_item = monkey.operate(item) // 3
            monkeys[monkey.throw(new_item)].items.append(new_item)
            monkey.items_inspected += 1
        monkey.items = []


def task(monkeys, n, mod):
    for _ in range(n):
        execute_round(monkeys, mod)
    items_inspected = sorted([monkey.items_inspected for monkey in monkeys])
    first = items_inspected.pop()
    second = items_inspected.pop()
    return first * second


def task1(monkeys):
    return task(monkeys, 20, None)


def task2(monkeys):
    mod = 1
    for monkey in monkeys:
        mod *= monkey.test_value
    return task(monkeys, 10000, mod)


def read_data(filename):
    with open(filename, 'rt') as file:
        monkey_codes = file.read().strip().split('\n\n')
    monkeys = []
    for monkey_code in monkey_codes:
        monkey = Monkey(monkey_code)
        monkeys.append(monkey)
    return monkeys


if __name__ == '__main__':
    data = read_data("day11.txt")
    print(task1(data))
    data = read_data("day11.txt")
    print(task2(data))
