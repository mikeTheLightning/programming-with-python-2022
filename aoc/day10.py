def get_x_after_cycle():
    """Generator continuously outputting x"""
    x = 1
    with open("day10.txt", "rt") as file:
        for i in file:
            tokens = i.strip().split()
            match tokens[0]:
                case "noop":
                    yield x
                case "addx":
                    yield x
                    yield x
                    x += int(tokens[1])


def task1():
    # part 1
    signal = 0
    for i, x in enumerate(get_x_after_cycle(), start=1):
        register_x = x
        if (i - 20) % 40 == 0:
            signal += register_x * i
    print(signal)


def task2():
    # part 2
    output = ""
    for i, x in enumerate(get_x_after_cycle(), start=0):
        i %= 40
        if x - 1 <= i <= x + 1:
            output += "#"
        else:
            output += "."
        if (i + 1) % 40 == 0:
            output += "\n"
    print(output)


if __name__ == '__main__':
    task1()
    task2()
