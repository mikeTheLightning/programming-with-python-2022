class Node:
    def __init__(self, elevation, pos, path_length=None):
        self.elevation = elevation
        self.pos = pos
        self.path_length = path_length


def out_of_bounds(i, j, matrix):
    """Checks if coordinate is outside the matrix"""
    return i < 0 or j < 0 or i >= len(matrix) or j >= len(matrix[0])


def search(start, end, edge_condition, matrix):
    # BFS
    todo = [start]
    start.path_length = 0
    while len(todo) != 0:
        current_node = todo.pop()
        # Try to move in every direction
        for x_diff, y_diff in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
            x, y = current_node.pos
            x_new, y_new = x + x_diff, y + y_diff
            if out_of_bounds(x_new, y_new, matrix) or not edge_condition(matrix[x_new][y_new].elevation,
                                                                         current_node.elevation):
                continue
            # Was this visited before?
            if not matrix[x_new][y_new].path_length:
                matrix[x_new][y_new].path_length = current_node.path_length + 1
                todo.insert(0, matrix[x_new][y_new])
                # Is this the end?
                if matrix[x_new][y_new] == end:
                    return


def find_best_starting_point(matrix):
    """Finds point with elevation 'a' from which the end is fastest to reach"""
    possible_points = []
    for row in matrix:
        for node in row:
            if node.elevation == 'a' and node.path_length:
                possible_points.append(node.path_length)
    return min(possible_points)


def parse_height_map(file_name):
    with open(file_name, 'rt') as file:
        text = file.readlines()
    matrix = []
    start = None
    end = None
    for i, line in enumerate(text):
        temp = []
        for j, letter in enumerate(line.strip()):
            if letter == 'E':
                end = Node('z', (i, j))
                temp.append(end)
            elif letter == 'S':
                start = Node('a', (i, j))
                temp.append(start)
            else:
                temp.append(Node(letter, (i, j)))
        matrix.append(temp)
    return matrix, start, end


def main():
    matrix, start, end = parse_height_map("day12.txt")
    search(start, end, lambda target, source: ord(target) - ord(source) <= 1, matrix)
    print(f"Task 1: {end.path_length}")

    matrix, start, end = parse_height_map("day12.txt")
    search(end, None, lambda target, source: ord(target) - ord(source) >= -1, matrix)
    print(f"Task 2: {find_best_starting_point(matrix)}")


if __name__ == '__main__':
    main()
