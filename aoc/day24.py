def move_monster(monsters, field):
    """Lets all monsters move one position"""
    new_positions = {}
    for (pre_x, pre_y), dirs in monsters.items():
        for directions in dirs:
            x, y = pre_x, pre_y
            match directions:
                case ">":
                    y = y + 1 if field[1] > y + 2 else 1
                case "<":
                    y = y - 1 if y > 1 else field[1] - 2
                case "v":
                    x = x + 1 if field[0] > x + 2 else 1
                case "^":
                    x = x - 1 if x > 1 else field[0] - 2
            if new_positions.get((x, y)):
                new_positions[x, y].append(directions)
            else:
                new_positions[x, y] = [directions]
    return new_positions


class Node:
    def __init__(self, pos: tuple[int, int], weight, pre=None):
        self.weight = weight
        self.position = pos
        self.pre = pre


def search(start: Node, end, states, monsters, field):
    """Implements BFS and returns the end node, with the corresponding weight (similar to day 12)"""
    todo = [start]
    visited = set()
    while len(todo) != 0:
        elem: Node = todo.pop()
        if (elem.position[0], elem.position[1], elem.weight % states) not in visited:
            visited.add((elem.position[0], elem.position[1], elem.weight % states))
            # Test if end reached
            if elem.position == end:
                return elem
            for direction in [(-1, 0), (0, 1), (1, 0), (0, -1), (0, 0)]:
                i, j = elem.position[0] + direction[0], elem.position[1] + direction[1]
                if (i < 1 or j < 1 or i >= field[0] - 1 or j >= field[1] - 1) and (i, j) != end and (
                        i, j) != start.position:
                    continue
                if monsters[(elem.weight + 1) % states].get((i, j)) is None:
                    todo.insert(0, Node((i, j), elem.weight + 1, elem))


def gcd(a, b):
    """Greatest common divisor"""
    if b == 0:
        return a
    else:
        return gcd(b, a % b)


def lcm(a, b):
    """Least common multiple"""
    return (a * b) // gcd(a, b)


def parse_monsters(input_file):
    """Parses the monsters and predicts their positions at every minute"""
    monsters = [{}]
    # Read file
    with open(input_file, "rt") as file:
        lines = file.read().strip().split("\n")
        field = len(lines), len(lines[0])
        print(field)
    for i, line in enumerate(lines[1:-1]):
        for j, x in enumerate(line[1:-1]):
            if x != ".":
                if monsters[0].get((i + 1, j + 1)):
                    monsters[0][i + 1, j + 1].append(x)
                else:
                    monsters[0][i + 1, j + 1] = [x]
    # For every minute predict monster postions (repeats after number states)
    number_states = lcm(field[0] - 2, field[1] - 2)
    for _ in range(number_states):
        m = move_monster(monsters[-1], field)
        monsters.append(m)
    return monsters, field, number_states


def main():
    # For every position store the corresponding monsters
    monsters, field, number_states = parse_monsters("day24.txt")

    # Part 1
    start = Node((0, 1), 0)
    end = search(start, (field[0] - 2, field[1] - 1), number_states, monsters, field)
    print(f"Task 1: {end.weight}")

    # Part 2
    start_again = search(end, (0, 1), number_states, monsters, field)
    end_again = search(start_again, (field[0] - 1, field[1] - 2), number_states, monsters, field)
    print(f"Task 2: {end_again.weight}")


if __name__ == '__main__':
    main()
