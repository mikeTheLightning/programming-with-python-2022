sides = [(0, 0, 1), (0, 1, 0), (1, 0, 0), (0, 0, -1), (0, -1, 0), (-1, 0, 0)]


def find_connected_component(cubes, v0):
    """Return connected component containing v0 from lecture"""
    discovered, discovered_set, i = [v0], {v0}, 0
    while i < len(discovered):
        v = discovered[i]
        for s in sides:
            candidate = tuple(c + s for c, s in zip(v, s))
            if candidate in cubes or candidate[0] not in range(x_min, x_max + 1) \
                    or candidate[1] not in range(y_min, y_max + 1) or candidate[2] not in range(z_min, z_max + 1):
                continue
            if candidate not in discovered_set:
                discovered.append(candidate)
                discovered_set.add(candidate)
        i += 1
    return discovered_set


def read_cubes(input_file):
    with open(input_file, "rt") as file:
        cubes = frozenset(tuple(map(int, line.split(','))) for line in file)
    return cubes


def count_exposed(cubes, search_space=None):
    """Count all exposed cubes. If given parameter search_space specifies which neighbours to consider"""
    exposed = 0
    for cube in cubes:
        for side in sides:
            neighbour = tuple(c + s for c, s in zip(cube, side))
            if neighbour not in cubes:
                if search_space:
                    if neighbour in search_space:
                        exposed += 1
                else:
                    exposed += 1
    return exposed


if __name__ == '__main__':
    # Part 1

    cubes = read_cubes("day18.txt")
    print(count_exposed(cubes))

    # Part 2
    # Build a frame around the structure
    x_min, y_min, z_min = min(map(lambda p: p[0], cubes)) - 1, min(map(lambda p: p[1], cubes)) - 1, min(
        map(lambda p: p[2], cubes)) - 1
    x_max, y_max, z_max = max(map(lambda p: p[0], cubes)) + 1, max(map(lambda p: p[1], cubes)) + 1, max(
        map(lambda p: p[2], cubes)) + 1

    # Cubes of air around lava
    outside = find_connected_component(cubes, (x_min, y_max, z_min))

    # Count cubes of lava next to air
    print(count_exposed(cubes, outside))
