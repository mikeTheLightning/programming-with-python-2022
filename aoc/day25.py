d_SNAFU = {2: "2", 1: "1", 0: "0", -1: "-", -2: "="}
SNAFU_d = {k: x for x, k in d_SNAFU.items()}


def decimal_to_snafu(x):
    """Converts from decimal to SNAFU"""
    digits_needed = 0
    for i in range(x):
        if x // (5 ** i) == 0:
            digits_needed = i
            break
    res = ""
    for j in reversed(range(digits_needed)):
        # Round because also negative numbers are possible
        n = round(x / (5 ** j))
        x -= n * (5 ** j)
        res += d_SNAFU[n]
    return res


def snafu_to_decimal(x):
    """Converts form decimal to SNAFU"""
    res = 0
    for i, c in enumerate(reversed(x)):
        res += SNAFU_d[c] * 5 ** i
    return res


def main():
    with open("day25.txt", "rt") as file:
        print(decimal_to_snafu(sum(snafu_to_decimal(line.strip()) for line in file)))


if __name__ == '__main__':
    main()
