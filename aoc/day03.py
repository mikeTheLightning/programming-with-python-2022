def priority(character):
    if ord(character) < ord('a'):
        return ord(character) - ord('A') + 27
    else:
        return ord(character) - ord('a') + 1


def extract_item(string):
    n = len(string)
    l_items = set(string[:n // 2])
    r_items = set(string[n // 2:])
    return list(l_items.intersection(r_items))[0]


def extract_common_item(a, b, c):
    return list(set(a).intersection(set(b)).intersection(set(c)))[0]


def parse_items():
    with open('day03.txt', 'rt') as file:
        lines = file.readlines()
    return lines


def task1():
    points = 0
    for line in parse_items():
        points += priority(extract_item(line.strip()))
    print(f"Task 1: {points}")


def task2():
    points2 = 0
    test = list(map(lambda x: x.strip(), parse_items()))
    for i in range(0, len(test), 3):
        elf1 = test[i]
        elf2 = test[i + 1]
        elf3 = test[i + 2]
        points2 += priority(extract_common_item(elf1, elf2, elf3))
    print(f"Task 2: {points2}")


if __name__ == '__main__':
    task1()
    task2()
