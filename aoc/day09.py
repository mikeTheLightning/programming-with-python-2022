def move_head(x, y, direction):
    match direction:
        case "U":
            return x, y + 1
        case "R":
            return x + 1, y
        case "D":
            return x, y - 1
        case "L":
            return x - 1, y


def follow(a, a_head):
    diff = a_head - a
    if diff > 0:
        return 1
    elif diff < 0:
        return -1
    else:
        return 0


def move_tail(x, y, x_head, y_head):
    x_dist = x_head - x
    y_dist = y_head - y
    if abs(x_dist) > 1 or abs(y_dist) > 1:
        return x + follow(x, x_head), y + follow(y, y_head)
    else:
        return x, y


def execute_moves(moves, snake_length):
    # Store head position
    x_head = y_head = 0
    # Store tail position
    tails = [(0,0) for _ in range(snake_length)]
    visited = {(0,0)}
    # Execute Moves
    for move in moves:
        x_head, y_head = move_head(x_head, y_head, move)
        x_current, y_current = x_head, y_head
        # Move tail
        for i in range(len(tails)):
            tails[i] = move_tail(tails[i][0], tails[i][1], x_current, y_current)
            x_current, y_current = tails[i]
        if (x_current, y_current) not in visited:
            visited.add((x_current, y_current))
    return len(visited)


def read_data(filename):
    moves = []
    with open(filename, "rt") as file:
        text = file.read().strip().split("\n")
        for line in text:
            tokens = line.split()
            for _ in range(int(tokens[1])):
                moves.append(tokens[0])
    return moves


if __name__ == '__main__':
    data = read_data("day09.txt")
    print(execute_moves(data, 1))
    print(execute_moves(data, 9))
