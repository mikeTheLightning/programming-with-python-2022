def is_max(other_numbers, number):
    if other_numbers:
        return max(other_numbers) < number
    else:
        return True


def is_visible(row, tree_index):
    tree = row[tree_index]
    return is_max(row[:tree_index], tree) or is_max(row[tree_index + 1:], tree)


def task1(trees):
    visible_trees = 0
    for x in range(len(trees)):
        for y in range(len(trees)):
            x_slice = trees[x]
            y_slice = [row[y] for row in trees]
            if is_visible(x_slice, y) or is_visible(y_slice, x):
                visible_trees += 1
    return visible_trees


def scenic_one(other_trees, tree):
    counter = 1
    for other_tree in other_trees:
        if other_tree < tree:
            counter += 1
        else:
            return counter
    return len(other_trees)


def scenic_row(row, tree_index):
    tree = row[tree_index]
    left_trees = list(reversed((row[:tree_index])))
    right_trees = row[tree_index + 1:]
    score = scenic_one(left_trees, tree) * scenic_one(right_trees, tree)
    return score


def scenic_score(trees, x: int, y: int):
    x_slice = trees[y]
    y_slice = [row[x] for row in trees]
    return scenic_row(x_slice, x) * scenic_row(y_slice, y)


def task2(trees):
    highest_score = 0
    for x in range(len(trees)):
        for y in range(len(trees)):
            highest_score = max(scenic_score(trees, x, y), highest_score)
    return highest_score


if __name__ == '__main__':
    data = []
    with open("day08.txt", "rt") as file:
        for line in file:
            data.append([int(x) for x in line.strip()])
    assert data
    assert len(data) == len(data[0])
    print(task1(data))
    print(task2(data))
