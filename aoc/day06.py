def task1(text):
    for i in range(0, len(text)):
        if len(set(text[i:i + 4])) == 4:
            print(i + 4, text[i:i + 4])
            break


def task2(text):
    for i in range(0, len(text)):
        if len(set(text[i:i + 14])) == 14:
            print(i + 14, text[i:i + 14])
            break


if __name__ == '__main__':
    with open("day06.txt", "rt") as file:
        data = file.read().strip()
    task1(data)
    task2(data)
