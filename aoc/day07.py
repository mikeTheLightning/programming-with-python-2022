def parse_cd(directory_name: str, wd: list):
    if directory_name == "..":
        wd.pop()
    else:
        wd.append(directory_name)


def ls(wd: list, system: dict):
    current_directory = system
    for directory in wd:
        current_directory = current_directory[directory][1]
    return current_directory


def parse_line(input_str, wd, system, current_directory):
    tokens = input_str.split()
    match tokens[0]:
        case "$":
            match tokens[1]:
                case "cd":
                    parse_cd(tokens[2], working_directory)
                case "ls":
                    current_directory = ls(wd, system)
        case "dir":
            current_directory[tokens[1]] = [None, {}]
        case _:
            current_directory[tokens[1]] = [int(tokens[0]), None]
    return current_directory


def compute_directory_size(current_directory):
    size = 0
    for element in current_directory:
        if current_directory[element][0]:
            size += current_directory[element][0]
        else:
            current_directory[element][0] = compute_directory_size(current_directory[element][1])
            size += current_directory[element][0]
    return size


def sizes_to_dict(current_directory, sizes):
    for element in current_directory:
        sub_directory = current_directory[element][1]
        if sub_directory:
            name_element = element
            counter = 0
            while name_element in sizes:
                name_element = element + str(f"_{counter}")
                counter += 1
            sizes[name_element] = current_directory[element][0]
            sizes_to_dict(sub_directory, sizes)


if __name__ == '__main__':
    # read structure
    file_structure = {"/": [None, {}]}  # stores size and content
    working_directory = []
    current_dir = file_structure
    with open("day07.txt", "rt") as file:
        for line in file:
            current_dir = parse_line(line, working_directory, file_structure, current_dir)
    compute_directory_size(file_structure)

    # store sizes to dict
    directory_sizes = dict()
    sizes_to_dict(file_structure, directory_sizes)

    # part 1
    total_sum = sum([directory_sizes[e] for e in directory_sizes if directory_sizes[e] <= 100000])
    print(f"Total size of directories smaller than 100000: {total_sum}")

    # part 2
    space_needed = 30000000 - (70000000 - directory_sizes["/"])
    print(f"Space needed: {space_needed}")
    min_space = 70000000
    for directory in directory_sizes:
        element_size = directory_sizes[directory]
        if min_space >= element_size >= space_needed:
            min_space = element_size
    print(f"Min space freed: {min_space}")
