def read_stacks(stack_str):
    stacks = [[], [], [], [], [], [], [], [], []]
    for line in stack_str:
        letters = line[1::4]
        for i in range(len(letters)):
            if ord('A') <= ord(letters[i]) <= ord('Z'):
                if stacks[i]:
                    stacks[i].insert(0, letters[i])
                else:
                    stacks[i] = [letters[i]]
    return stacks


def execute_move(amount, source, destination, stacks, new):
    transfer = []
    for _ in range(amount):
        transfer += stacks[source].pop()
    # To enable 9001 functionality
    if new:
        transfer.reverse()
    stacks[destination] += transfer


def read_cargo():
    with open("day05.txt", "rt") as file:
        text = file.read()
    sections = text.split("\n\n")
    cargo_stacks = read_stacks(sections[0].split("\n")[:-1])
    move_lines = sections[1].split("\n")[:-1]
    return cargo_stacks, move_lines


def task1():
    cargo_stacks, move_lines = read_cargo()
    for line in move_lines:
        tokens = line.split()
        execute_move(int(tokens[1]), int(tokens[3]) - 1, int(tokens[5]) - 1, cargo_stacks, False)
    top_of_stack = "".join([i.pop() for i in cargo_stacks])
    print(f"Task 1: {top_of_stack}")


def task2():
    cargo_stacks, move_lines = read_cargo()
    for line in move_lines:
        tokens = line.split()
        execute_move(int(tokens[1]), int(tokens[3]) - 1, int(tokens[5]) - 1, cargo_stacks, True)
    top_of_stack = "".join([i.pop() for i in cargo_stacks])
    print(f"Task 2: {top_of_stack}")


if __name__ == '__main__':
    task1()
    task2()