def parse_path(rocks, path):
    corners = []

    # Extract the positions of all corners for this rock structure
    for pos_str in path.split(' -> '):
        x, y = [int(x) for x in pos_str.split(',')]
        corners.append((x, y))

    # Iterate over all corners
    for i in range(1, len(corners)):
        # Add all x coordinates
        for x in range(min(corners[i][0], corners[i - 1][0]), max(corners[i][0], corners[i - 1][0]) + 1):
            if rocks.get(x) is None:
                rocks[x] = set()
            rocks[x].add(corners[i][1])
        # For each x coordinate store the y values
        for y in range(min(corners[i][1], corners[i - 1][1]), max(corners[i][1], corners[i - 1][1]) + 1):
            if rocks.get(corners[i][0]) is None:
                rocks[corners[i][0]] = set()
            rocks[corners[i][0]].add(y)

    # Return maximal y for all corners (task2)
    return max(x[1] for x in corners)


def move_sand(rocks, floor=None):
    """Moves one sandblock in"""
    pos = (500, 0)
    while True:
        column = rocks.get(pos[0])
        # Test if something is in the way
        if column is None or max(column) <= pos[1]:
            if floor is None:
                return None
            else:
                if column is None:
                    rocks[pos[0]] = set()
                rocks[pos[0]].add(floor - 1)
                return pos[0], floor - 1
        else:
            # Search for highest rock
            y = min(filter(lambda y: y > pos[1], column))
            # Hit the highest rock
            pos = (pos[0], y - 1)
            # Flow left if possible
            if rocks.get(pos[0] - 1) is None or y not in rocks[pos[0] - 1]:
                pos = (pos[0] - 1, pos[1])
            # Flow right if possible
            elif rocks.get(pos[0] + 1) is None or y not in rocks[pos[0] + 1]:
                pos = (pos[0] + 1, pos[1])
            # No flowing possible
            else:
                rocks[pos[0]].add(pos[1])
                return pos


def sand(rocks, end=None, floor=None):
    """Processes sand flow and returns number of sand particles."""
    i = 0
    while move_sand(rocks, floor) != end:
        i += 1
    return i if floor is None else i + 1


def task1():
    with open("day14.txt", "rt") as file:
        rocks = {}
        for line in file:
            parse_path(rocks, line)
    print(sand(rocks))


def task2():
    with open("day14.txt", "rt") as file:
        rocks = {}
        max_y = 0
        for line in file:
            max_y = max(max_y, parse_path(rocks, line))
    print(sand(rocks, (500, 0), max_y + 2))


if __name__ == '__main__':
    task1()
    task2()