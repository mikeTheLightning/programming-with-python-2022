# Used to specify custom sorting function
import functools


def read_data(file_name):
    with open(file_name, "rt") as file:
        text = file.read()
    pairs_str = text.split("\n\n")
    pairs = []
    for pair_str in pairs_str:
        parts = pair_str.split()
        pair = eval(parts[0]), eval(parts[1])
        pairs.append(pair)
    return pairs


def cmp(left, right):
    """Custom list sorting function"""
    match left, right:
        case int(x), int(y):
            if x < y:
                return -1
            elif x > y:
                return 1
            else:
                return 0
        case [], []:
            return 0
        case [], [_, *_]:
            return -1
        case [_, *_], []:
            return 1
        case [x, *xs], [y, *ys]:
            first_cmp = cmp(x, y)
            if first_cmp == 0:
                return cmp(xs, ys)
            else:
                return first_cmp
        case [x], [y]:
            return cmp(x, y)
        case x, int(y):
            return cmp(x, [y])
        case int(x), y:
            return cmp([x], y)


def task1():
    pairs = read_data("day13.txt")
    counter = 0
    for i, (x, y) in enumerate(pairs, start=1):
        if cmp(x, y) == -1:
            counter += i
    return counter


def task2():
    pairs = read_data("day13.txt")
    packets = [packet for pair in pairs for packet in pair] + [[[2]]] + [[[6]]]
    packets_sorted = sorted(packets, key=functools.cmp_to_key(lambda x, y: cmp(x, y)))
    index_a = packets_sorted.index([[2]]) + 1
    index_b = packets_sorted.index([[6]]) + 1
    return index_a * index_b


if __name__ == '__main__':
    print(task1())
    print(task2())
