def task1():
    with open("day01.txt", "rt") as file:
        highest_calories = 0
        current_calories = 0
        for line in file:
            if line == "\n":
                highest_calories = max(highest_calories, current_calories)
                current_calories = 0
            else:
                current_calories += int(line.strip())
        highest_calories = max(highest_calories, current_calories)
        print(f"Calories of top elv: {highest_calories}")


def task2():
    with open("day01.txt", "rt") as file:
        highest_calories = [0, 0, 0]
        current_calories = 0
        for line in file:
            if line == "\n":
                highest_calories.sort()
                highest_calories[0] = max(highest_calories[0], current_calories)
                current_calories = 0
            else:
                current_calories += int(line.strip())
        highest_calories[0] = max(highest_calories[0], current_calories)
        print(f"Calories of top three elves: {sum(highest_calories)}")


if __name__ == '__main__':
    task1()
    task2()
