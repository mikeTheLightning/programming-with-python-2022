shape_score = {'A': 1, 'B': 2, 'C': 3}
beats = {'A': 'B', 'B': 'C', 'C': 'A'}
loses = {'A': 'C', 'B': 'A', 'C': 'B'}
translate = {'X': 'A', 'Y': 'B', 'Z': 'C'}


def what_to_choose(opponent, instruction):
    match instruction:
        case 'X':
            me = loses[opponent]
        case 'Y':
            me = opponent
        case 'Z':
            me = beats[opponent]
        case _:
            raise AttributeError("This is no result")
    return me


def calculate_outcome_score(opponent, me):
    if me in beats[opponent]:
        return 6
    elif me in loses[opponent]:
        return 0
    else:
        return 3


def calculate_total_score(opponent, me):
    return calculate_outcome_score(opponent, me) + shape_score[me]


def task1():
    total_score = 0
    with open('day02.txt', 'rt') as file:
        for line in file:
            opponent_chooses, me_chooses = line.split()
            total_score += calculate_total_score(opponent_chooses, translate[me_chooses])
    print(f"The total score is: {total_score}")


def task2():
    total_score = 0
    with open('day02.txt', 'rt') as file:
        for line in file:
            opponent_chooses, result = line.split()
            me_chooses = what_to_choose(opponent_chooses, result)
            total_score += calculate_total_score(opponent_chooses, me_chooses)
    print(f"The total score is: {total_score}")


if __name__ == '__main__':
    task1()
    task2()
