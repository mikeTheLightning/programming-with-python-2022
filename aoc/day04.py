import re


def detect_inclusion(outer_left, outer_right, inner_left, inner_right):
    return outer_left <= inner_left and inner_right <= outer_right


def detect_overlap(left_l, right_l, left_r, right_r):
    return left_l <= left_r <= right_l


if __name__ == '__main__':
    with open('day04.txt') as file:
        counter = 0
        counter_overlap = 0
        for line in file:
            left1, right1, left2, right2 = map(int, re.split('-|,', line))
            counter += detect_inclusion(left1, right1, left2, right2) or detect_inclusion(left2, right2, left1, right1)
            counter_overlap += detect_overlap(left1, right1, left2, right2) or detect_overlap(left2, right2, left1,
                                                                                              right1)
    print(f"Task 1: {counter}")
    print(f"Task 2: {counter_overlap}")
