# Used to solve and execute mathematical instructions
from sympy import Symbol, solve, sympify, lambdify


def equalation(monkey: str, values, humn):
    if monkey == "humn":
        return humn
    match values[monkey]:
        case [op_l, "/", op_r]:
            return sympify(equalation(op_l, values, humn) / equalation(op_r, values, humn), locals=humn)
        case [op_l, "*", op_r]:
            return sympify(equalation(op_l, values, humn) * equalation(op_r, values, humn), locals=humn)
        case [op_l, "+", op_r]:
            return sympify(equalation(op_l, values, humn) + equalation(op_r, values, humn), locals=humn)
        case [op_l, "-", op_r]:
            return sympify(equalation(op_l, values, humn) - equalation(op_r, values, humn), locals=humn)
        case [number]:
            return sympify(number)


def read_data(file_name):
    values: dict = {}
    with open(file_name, "rt") as file:
        for line in file:
            tokens = line.split()
            monkey_name = tokens[0][:-1]
            values[monkey_name] = tokens[1:]
    return values


def main():
    humn = Symbol("humn")
    values = read_data("day21.txt")
    e1 = equalation(values["root"][0], values, humn)
    e2 = equalation(values["root"][-1], values, humn)
    print("Task 1:", int(lambdify(humn, e1 + e2)(sympify(values["humn"][0]))))
    print("Task 2:", solve(e1 - e2, humn)[0])


if __name__ == '__main__':
    main()
